package ru.fadeev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fadeev.tm.api.endpoint.Exception_Exception;
import ru.fadeev.tm.api.endpoint.User;
import ru.fadeev.tm.util.PasswordHashUtil;

import java.util.UUID;

@Category(IntegrationTest.class)
public class SessionEndpointTest extends AbstractTest {

    @Test
    public void getTokenTest() throws Exception {
        @NotNull User user = new User();
        user.setLogin(UUID.randomUUID().toString());
        user.setPasswordHash(PasswordHashUtil.md5("1"));
        userEndpoint.persistUser(user);
        @NotNull final String tokenTrue = sessionEndpoint.getToken(user.getLogin(), "1");
        Assert.assertTrue(!tokenTrue.isEmpty());
        sessionEndpoint.closeSession(tokenTrue);
        @NotNull String tokenFalse = null;
        try {
            tokenFalse = sessionEndpoint.getToken(user.getLogin(), "2");
        } catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains("Bad login or password"));
        }
        Assert.assertNull(tokenFalse);
        user = userEndpoint.findUserByLoginUser(tokenAdmin, user.getLogin());
        userEndpoint.removeUser(tokenAdmin, user.getId());
    }

    @Test
    public void closeSessionTest() throws Exception_Exception {
        @NotNull User user = new User();
        user.setLogin(UUID.randomUUID().toString());
        user.setPasswordHash(PasswordHashUtil.md5("1"));
        userEndpoint.persistUser(user);
        @NotNull final String token = sessionEndpoint.getToken(user.getLogin(), "1");
        user = userEndpoint.findOneUser(token);
        sessionEndpoint.closeSession(token);
        @Nullable User testUser = null;
        try {
            testUser = userEndpoint.findOneUser(token);
        }
        catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains("Invalid session"));
        }
        Assert.assertNull(testUser);
        userEndpoint.removeUser(tokenAdmin,user.getId());
    }

}
