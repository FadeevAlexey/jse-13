package ru.fadeev.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.fadeev.tm.api.endpoint.Exception_Exception;
import ru.fadeev.tm.api.endpoint.Project;
import ru.fadeev.tm.api.endpoint.Status;
import ru.fadeev.tm.constant.DateConst;
import ru.fadeev.tm.util.DateUtil;

import java.util.List;

public class ProjectEndpointTest extends AbstractTest {

    @Test
    public void persistProjectTest() throws Exception {
        @NotNull Project project = new Project();
        project.setName("testUser");
        projectEndpoint.persistProject(tokenUser, project);
        Assert.assertNotNull(projectEndpoint.findIdByNameProject(tokenUser, "testUser"));
    }

    @Test
    public void findAllProjectTest() throws Exception {
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        @NotNull Project project3 = new Project();
        projectEndpoint.persistProject(tokenUser, project);
        projectEndpoint.persistProject(tokenUser, project2);
        projectEndpoint.persistProject(tokenAdmin, project3);
        List<Project> userProjects = projectEndpoint.findAllProject(tokenUser);
        Assert.assertTrue(userProjects.size() == 2);
        List<Project> adminProjects = projectEndpoint.findAllProject(tokenAdmin);
        Assert.assertTrue(adminProjects.size() == 1);
    }

    @Test
    public void removeAllProjectTest() throws Exception {
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        @NotNull Project project3 = new Project();
        projectEndpoint.persistProject(tokenUser, project);
        projectEndpoint.persistProject(tokenUser, project2);
        projectEndpoint.persistProject(tokenAdmin, project3);
        projectEndpoint.removeAllProject(tokenUser);
        projectEndpoint.removeAllProject(tokenAdmin);
        List<Project> userProjects = projectEndpoint.findAllProject(tokenUser);
        Assert.assertTrue(userProjects.size() == 0);
        List<Project> adminProjects = projectEndpoint.findAllProject(tokenAdmin);
        Assert.assertTrue(adminProjects.size() == 0);
    }

    @Test
    public void findIdByNameProjectTest() throws Exception {
        @NotNull Project project = new Project();
        project.setName("test");
        projectEndpoint.persistProject(tokenUser, project);
        @NotNull final String id = projectEndpoint.findIdByNameProject(tokenUser, "test");
        @NotNull final Project testProject = projectEndpoint.findOneProject(tokenUser, id);
        Assert.assertEquals(project.getName(), testProject.getName());
        Assert.assertNull(projectEndpoint.findOneProject(tokenAdmin, "test"));
    }

    @Test
    public void findOneProjectTest() throws Exception {
        @NotNull Project project = new Project();
        project.setName("test");
        projectEndpoint.persistProject(tokenUser, project);
        @NotNull final String id = projectEndpoint.findIdByNameProject(tokenUser, "test");
        @NotNull final Project testProject = projectEndpoint.findOneProject(tokenUser, id);
        Assert.assertEquals(project.getName(), testProject.getName());
        Assert.assertNull(projectEndpoint.findOneProject(tokenAdmin, "test"));
    }

    @Test
    public void mergeProjectTest() throws Exception_Exception {
        @NotNull Project project = new Project();
        project.setName("test");
        projectEndpoint.persistProject(tokenUser, project);
        @NotNull final String id = projectEndpoint.findIdByNameProject(tokenUser, "test");
        @NotNull Project testProject = projectEndpoint.findOneProject(tokenUser, id);
        testProject.setDescription("test description");
        projectEndpoint.mergeProject(tokenUser, testProject);
        testProject = projectEndpoint.findOneProject(tokenUser, id);
        Assert.assertEquals(testProject.getDescription(), "test description");
        testProject.setDescription("admin test");
        projectEndpoint.mergeProject(tokenAdmin, testProject);
        testProject = projectEndpoint.findOneProject(tokenUser, id);
        Assert.assertEquals("test description", testProject.getDescription());
    }

    @Test
    public void searchByDescriptionTest() throws Exception_Exception {
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        @NotNull Project project3 = new Project();
        project.setName("test1");
        project2.setName("test2");
        project3.setName("test3");
        project.setDescription("ale");
        project2.setDescription("pale");
        project3.setDescription("vova");
        projectEndpoint.persistProject(tokenUser, project);
        projectEndpoint.persistProject(tokenUser, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        List<Project> projects = projectEndpoint.searchByDescriptionProject(tokenUser, "ale");
        Assert.assertEquals(2, projects.size());
        for (Project testProject : projects)
            Assert.assertTrue(
                    testProject.getDescription().equals("ale") ||
                            testProject.getDescription().equals("pale")
            );
    }

    @Test
    public void searchByNameTest() throws Exception {
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        @NotNull Project project3 = new Project();
        project.setName("ale");
        project2.setName("pale");
        project3.setName("vova");
        projectEndpoint.persistProject(tokenUser, project);
        projectEndpoint.persistProject(tokenUser, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        List<Project> projects = projectEndpoint.searchByNameProject(tokenUser, "ale");
        Assert.assertEquals(2, projects.size());
        for (Project testProject : projects)
            Assert.assertTrue(
                    testProject.getName().equals("ale") ||
                            testProject.getName().equals("pale")
            );
    }

    @Test
    public void removeProjectTest() throws Exception {
        @NotNull Project project = new Project();
        project.setName("test");
        projectEndpoint.persistProject(tokenUser, project);
        @NotNull String projectId = projectEndpoint.findIdByNameProject(tokenUser,"test");
        projectEndpoint.removeProject(tokenAdmin,projectId);
        Assert.assertEquals("test", projectEndpoint.findOneProject(tokenUser, projectId).getName());
        projectEndpoint.removeProject(tokenUser,projectId);
        Assert.assertNull(projectEndpoint.findOneProject(tokenUser, projectId));

    }

    @Test
    public void sortAllProjectByStatusTest() throws Exception_Exception {
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        @NotNull Project project3 = new Project();
        project.setStatus(Status.IN_PROGRESS);
        project2.setStatus(Status.DONE);
        projectEndpoint.persistProject(tokenUser, project);
        projectEndpoint.persistProject(tokenUser, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        List<Project> projects = projectEndpoint.sortByStatusProject(tokenUser);
        Assert.assertEquals(Status.DONE,projects.get(0).getStatus());
        Assert.assertEquals(Status.PLANNED,projects.get(2).getStatus());
        List<Project> projectsAdmin = projectEndpoint.sortByStatusProject(tokenAdmin);
        Assert.assertEquals(0,projectsAdmin.size());
    }

    @Test
    public void sortAllProjectByStartDateTest() throws Exception {
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        @NotNull Project project3 = new Project();
        project.setName("Project");
        project2.setName("Project2");
        project3.setName("Project3");
        project.setStartDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2010")));
        project2.setStartDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2000")));
        project3.setStartDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2020")));
        projectEndpoint.persistProject(tokenUser, project);
        projectEndpoint.persistProject(tokenUser, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        List<Project> projects = projectEndpoint.sortByStartDateProject(tokenUser);
        Assert.assertEquals("Project2",projects.get(0).getName());
        Assert.assertEquals("Project3",projects.get(2).getName());
        List<Project> projectsAdmin = projectEndpoint.sortByStartDateProject(tokenAdmin);
        Assert.assertEquals(0,projectsAdmin.size());
    }

    @Test
    public void sortAllProjectByFinishDateTest() throws Exception {
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        @NotNull Project project3 = new Project();
        project.setName("Project");
        project2.setName("Project2");
        project3.setName("Project3");
        project.setFinishDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2020")));
        project2.setFinishDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2000")));
        project3.setFinishDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2010")));
        projectEndpoint.persistProject(tokenUser, project);
        projectEndpoint.persistProject(tokenUser, project2);
        projectEndpoint.persistProject(tokenUser, project3);
        List<Project> projects = projectEndpoint.sortByFinishDateProject(tokenUser);
        Assert.assertEquals("Project2",projects.get(0).getName());
        Assert.assertEquals("Project3",projects.get(1).getName());
        List<Project> projectsAdmin = projectEndpoint.sortByFinishDateProject(tokenAdmin);
        Assert.assertEquals(0,projectsAdmin.size());
    }

    @Test
    public void sortAllProjectByCreationTest() throws Exception {
        @NotNull Project project = new Project();
        project.setName("Project");
        projectEndpoint.persistProject(tokenUser, project);
        @NotNull Project project2 = new Project();
        project2.setName("Project2");
        Thread.sleep(1000);
        projectEndpoint.persistProject(tokenUser, project2);
        @NotNull Project project3 = new Project();
        project3.setName("Project3");
        Thread.sleep(1000);
        projectEndpoint.persistProject(tokenUser, project3);
        List<Project> projects = projectEndpoint.sortByCreationTimeProject(tokenUser);
        Assert.assertEquals("Project",projects.get(0).getName());
        Assert.assertEquals("Project3",projects.get(2).getName());
        List<Project> projectsAdmin = projectEndpoint.sortByCreationTimeProject(tokenAdmin);
        Assert.assertEquals(0,projectsAdmin.size());
    }


}
