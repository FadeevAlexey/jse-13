package ru.fadeev.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fadeev.tm.api.endpoint.Exception_Exception;
import ru.fadeev.tm.api.endpoint.Project;
import ru.fadeev.tm.api.endpoint.Status;
import ru.fadeev.tm.api.endpoint.Task;
import ru.fadeev.tm.constant.DateConst;
import ru.fadeev.tm.util.DateUtil;

import java.util.List;
import java.util.UUID;

@Category(IntegrationTest.class)
public class TaskEndpointTest extends AbstractTest {

    @Test
    public void persistTaskTest() throws Exception {
        @NotNull Task task = new Task();
        task.setName("testUser");
        taskEndpoint.persistTask(tokenUser, task);
        Assert.assertNotNull(taskEndpoint.findIdByNameTask(tokenUser, "testUser"));
    }

    @Test
    public void findAllTaskTest() throws Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenAdmin, task3);
        List<Task> tasks = taskEndpoint.findAllTask(tokenUser);
        Assert.assertEquals(2, tasks.size());
        List<Task> adminTasks = taskEndpoint.findAllTask(tokenAdmin);
        Assert.assertEquals(1, adminTasks.size());
    }

    @Test
    public void removeAllTaskTest() throws Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenAdmin, task3);
        taskEndpoint.removeAllTask(tokenUser);
        taskEndpoint.removeAllTask(tokenAdmin);
        List<Task> tasks = taskEndpoint.findAllTask(tokenUser);
        Assert.assertTrue(tasks.size() == 0);
        List<Task> adminTasks = taskEndpoint.findAllTask(tokenAdmin);
        Assert.assertTrue(adminTasks.size() == 0);
    }

    @Test
    public void findIdByNameTaskTest() throws Exception {
        @NotNull Task task = new Task();
        task.setName("test");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull final String id = taskEndpoint.findIdByNameTask(tokenUser, "test");
        @NotNull final Task testTask = taskEndpoint.findOneTask(tokenUser, id);
        Assert.assertEquals(task.getName(), testTask.getName());
        Assert.assertNull(taskEndpoint.findOneTask(tokenAdmin, "test"));
    }

    @Test
    public void findOneTaskTest() throws Exception {
        @NotNull Task task = new Task();
        task.setName("test");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull final String id = taskEndpoint.findIdByNameTask(tokenUser, "test");
        @NotNull final Task testTask = taskEndpoint.findOneTask(tokenUser, id);
        Assert.assertEquals(task.getName(), testTask.getName());
        Assert.assertNull(taskEndpoint.findOneTask(tokenAdmin, "test"));
    }

    @Test
    public void mergeTaskTest() throws Exception_Exception {
        @NotNull Task task = new Task();
        task.setName("test");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull final String id = taskEndpoint.findIdByNameTask(tokenUser, "test");
        @NotNull Task testTask = taskEndpoint.findOneTask(tokenUser, id);
        testTask.setDescription("test description");
        taskEndpoint.mergeTask(tokenUser, testTask);
        testTask = taskEndpoint.findOneTask(tokenUser, id);
        Assert.assertEquals(testTask.getDescription(), "test description");
        testTask.setDescription("admin test");
        taskEndpoint.mergeTask(tokenAdmin, testTask);
        testTask = taskEndpoint.findOneTask(tokenUser, id);
        Assert.assertEquals("test description", testTask.getDescription());
    }

    @Test
    public void searchByDescriptionTaskTest() throws Exception_Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        task.setName("test1");
        task2.setName("test2");
        task3.setName("test3");
        task.setDescription("ale");
        task2.setDescription("pale");
        task3.setDescription("vova");
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<Task> tasks = taskEndpoint.searchByDescriptionTask(tokenUser, "ale");
        Assert.assertEquals(2, tasks.size());
        for (Task testTask : tasks)
            Assert.assertTrue(
                    testTask.getDescription().equals("ale") ||
                            testTask.getDescription().equals("pale")
            );
    }

    @Test
    public void searchByNameTaskTest() throws Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        task.setName("ale");
        task2.setName("pale");
        task3.setName("vova");
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<Task> tasks = taskEndpoint.searchByNameTask(tokenUser, "ale");
        Assert.assertEquals(2, tasks.size());
        for (Task testTasks : tasks)
            Assert.assertTrue(
                    testTasks.getName().equals("ale") ||
                            testTasks.getName().equals("pale")
            );
    }

    @Test
    public void removeTaskTest() throws Exception {
        @NotNull Task task = new Task();
        task.setName("test");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull String taskId = taskEndpoint.findIdByNameTask(tokenUser,"test");
        taskEndpoint.removeTask(tokenAdmin,taskId);
        Assert.assertEquals("test", taskEndpoint.findOneTask(tokenUser, taskId).getName());
        taskEndpoint.removeTask(tokenUser,taskId);
        Assert.assertNull(taskEndpoint.findOneTask(tokenUser, taskId));

    }

    @Test
    public void removeAllByProjectIdTaskTest() throws Exception_Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        @NotNull final String testProjectId = UUID.randomUUID().toString();
        @NotNull Project project = new Project();
        project.setId(testProjectId);
        task.setProjectId(testProjectId);
        task2.setProjectId(testProjectId);
        projectEndpoint.persistProject(tokenUser,project);
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        taskEndpoint.removeAllByProjectIdTask(tokenAdmin, testProjectId);
        Assert.assertEquals(3,taskEndpoint.findAllTask(tokenUser).size());
        taskEndpoint.removeAllByProjectIdTask(tokenUser,testProjectId);
        Assert.assertEquals(1,taskEndpoint.findAllTask(tokenUser).size());
    }

    @Test
    public void removeAllByProjectTaskTest() throws Exception_Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        @NotNull Task task4 = new Task();
        @NotNull Project project = new Project();
        project.setId(UUID.randomUUID().toString());
        @NotNull Project project2 = new Project();
        project2.setId(UUID.randomUUID().toString());
        task.setProjectId(project.getId());
        task2.setProjectId(project.getId());
        task3.setProjectId(project2.getId());
        projectEndpoint.persistProject(tokenUser,project);
        projectEndpoint.persistProject(tokenUser,project2);
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        taskEndpoint.persistTask(tokenUser,task4);
        taskEndpoint.removeAllProjectTask(tokenUser);
        Assert.assertEquals(1,taskEndpoint.findAllTask(tokenUser).size());
    }

    @Test
    public void findAllByTaskIdTest() throws Exception_Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        @NotNull Task task4 = new Task();
        @NotNull Project project = new Project();
        project.setId(UUID.randomUUID().toString());
        @NotNull Project project2 = new Project();
        project2.setId(UUID.randomUUID().toString());
        task.setProjectId(project.getId());
        task2.setProjectId(project.getId());
        task3.setProjectId(project2.getId());
        projectEndpoint.persistProject(tokenUser,project);
        projectEndpoint.persistProject(tokenUser,project2);
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        taskEndpoint.persistTask(tokenUser,task4);
        List<Task> tasks = taskEndpoint.findAllByProjectIdTask(tokenUser,project.getId());
        Assert.assertEquals(2,tasks.size());
        List<Task> tasksAdmin = taskEndpoint.findAllByProjectIdTask(tokenAdmin,project.getId());
        Assert.assertEquals(0,tasksAdmin.size());
    }

    @Test
    public void sortAllTaskByStatusTest() throws Exception_Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        task.setStatus(Status.IN_PROGRESS);
        task2.setStatus(Status.DONE);
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<Task> tasks = taskEndpoint.sortByStatusTask(tokenUser);
        Assert.assertEquals(Status.DONE,tasks.get(0).getStatus());
        Assert.assertEquals(Status.PLANNED,tasks.get(2).getStatus());
        List<Task> tasksAdmin = taskEndpoint.sortByStatusTask(tokenAdmin);
        Assert.assertEquals(0,tasksAdmin.size());
    }

    @Test
    public void sortAllTaskByStartDateTest() throws Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        task.setName("Task");
        task2.setName("Task2");
        task3.setName("Task3");
        task.setStartDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2010")));
        task2.setStartDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2000")));
        task3.setStartDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2020")));
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<Task> tasks = taskEndpoint.sortByStartDateTask(tokenUser);
        Assert.assertEquals("Task2",tasks.get(0).getName());
        Assert.assertEquals("Task3",tasks.get(2).getName());
        List<Task> tasksAdmin = taskEndpoint.sortByStartDateTask(tokenAdmin);
        Assert.assertEquals(0,tasksAdmin.size());
    }

    @Test
    public void sortAllTaskByFinishDateTest() throws Exception {
        @NotNull Task task = new Task();
        @NotNull Task task2 = new Task();
        @NotNull Task task3 = new Task();
        task.setName("Task");
        task2.setName("Task2");
        task3.setName("Task3");
        task.setFinishDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2020")));
        task2.setFinishDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2000")));
        task3.setFinishDate(DateUtil.calendarConverter(DateConst.DATE_FORMAT.parse("10.10.2010")));
        taskEndpoint.persistTask(tokenUser, task);
        taskEndpoint.persistTask(tokenUser, task2);
        taskEndpoint.persistTask(tokenUser, task3);
        List<Task> tasks = taskEndpoint.sortByFinishDateTask(tokenUser);
        Assert.assertEquals("Task2",tasks.get(0).getName());
        Assert.assertEquals("Task3",tasks.get(1).getName());
        List<Task> tasksAdmin = taskEndpoint.sortByFinishDateTask(tokenAdmin);
        Assert.assertEquals(0,tasksAdmin.size());
    }

    @Test
    public void sortAllTaskByCreationTest() throws Exception {
        @NotNull Task task = new Task();
        task.setName("Task");
        taskEndpoint.persistTask(tokenUser, task);
        @NotNull Task task2 = new Task();
        task2.setName("Task2");
        Thread.sleep(1000);
        taskEndpoint.persistTask(tokenUser, task2);
        @NotNull Task task3 = new Task();
        task3.setName("Task3");
        Thread.sleep(1000);
        taskEndpoint.persistTask(tokenUser, task3);
        List<Task> tasks = taskEndpoint.sortByCreationTimeTask(tokenUser);
        Assert.assertEquals("Task",tasks.get(0).getName());
        Assert.assertEquals("Task3",tasks.get(2).getName());
        List<Task> tasksAdmin = taskEndpoint.sortByCreationTimeTask(tokenAdmin);
        Assert.assertEquals(0,tasksAdmin.size());
    }

}
