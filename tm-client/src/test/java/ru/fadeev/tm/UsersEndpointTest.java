package ru.fadeev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import ru.fadeev.tm.api.endpoint.Exception_Exception;
import ru.fadeev.tm.api.endpoint.User;
import ru.fadeev.tm.util.PasswordHashUtil;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Category(IntegrationTest.class)
public class UsersEndpointTest extends AbstractTest {

    @Test
    public void findOneUserTest() throws Exception {
        User user = userEndpoint.findOneUser(tokenUser);
        User admin = userEndpoint.findOneUser(tokenAdmin);
        Assert.assertEquals("User",user.getLogin());
        Assert.assertEquals("Admin", admin.getLogin());
    }

    @Test
    public void isLoginExistUserTest() throws Exception {
        boolean userExist = userEndpoint.isLoginExistUser("User");
        boolean adminExist = userEndpoint.isLoginExistUser("Admin");
        boolean randomExist = userEndpoint.isLoginExistUser(UUID.randomUUID().toString());
        Assert.assertTrue(userExist);
        Assert.assertTrue(adminExist);
        Assert.assertFalse(randomExist);
    }

    @Test
    public void persistUserTest() throws Exception {
        @NotNull User user = new User();
        user.setLogin(UUID.randomUUID().toString());
        userEndpoint.persistUser(user);
        user = userEndpoint.findUserByLoginUser(tokenAdmin, user.getLogin());
        Assert.assertTrue(userEndpoint.isLoginExistUser(user.getLogin()));
        userEndpoint.removeUser(tokenAdmin,user.getId());
        Assert.assertFalse(userEndpoint.isLoginExistUser(user.getLogin()));
    }

    @Test
    public void mergeUserTest() throws Exception {
        @NotNull User user = new User();
        user.setLogin(UUID.randomUUID().toString());
        user.setPasswordHash(PasswordHashUtil.md5("1"));
        userEndpoint.persistUser(user);
        user = userEndpoint.findUserByLoginUser(tokenAdmin, user.getLogin());
        String token = sessionEndpoint.getToken(user.getLogin(),"1");
        @NotNull final String randomString = UUID.randomUUID().toString();
        user.setLogin(randomString);
        try{
            userEndpoint.mergeUser(tokenUser,user);
        } catch (Exception e){
            Assert.assertTrue(e.getMessage().contains("Access denied"));
        }
        userEndpoint.mergeUser(token,user);
        user = userEndpoint.findUserByLoginUser(tokenAdmin,randomString);
        Assert.assertNotNull(user);
        Assert.assertEquals(randomString,user.getLogin());
        getSessionEndpoint().closeSession(token);
        userEndpoint.removeUser(tokenAdmin,user.getId());
    }

    @Test
    public void findAllUsersTest() throws Exception_Exception {
        List<User> users = userEndpoint.findAllUser(tokenAdmin);
       int userCount =  users
                .stream()
                .filter(user -> user.getLogin().equals("Admin") || user.getLogin().equals("User"))
                .collect(Collectors.toList())
                .size();
       Assert.assertEquals(userCount,2);
    }

    @Test
    public void removeUsersTest() throws Exception_Exception {
        @NotNull final String userRandomLogin = UUID.randomUUID().toString();
        @NotNull User user = new User();
        user.setLogin(userRandomLogin);
        user.setPasswordHash(PasswordHashUtil.md5("1"));
        userEndpoint.persistUser(user);
        user = userEndpoint.findUserByLoginUser(tokenAdmin, user.getLogin());
        Assert.assertEquals(userRandomLogin,user.getLogin());
        try {
            userEndpoint.removeUser(tokenUser,user.getId());
        }catch (Exception e) {
            Assert.assertTrue(e.getMessage().contains("Access denied"));
        }
        boolean userExistTrue = userEndpoint.isLoginExistUser(user.getLogin());
        Assert.assertTrue(userExistTrue);
        userEndpoint.removeUser(getTokenAdmin(),user.getId());
        boolean userExistFalse = userEndpoint.isLoginExistUser(user.getLogin());
        Assert.assertFalse(userExistFalse);
    }

    @Test
    public void findUserByLoginTest() throws Exception_Exception {
        @NotNull User user = userEndpoint.findUserByLoginUser(tokenAdmin,"User");
        Assert.assertEquals("User", user.getLogin());
        @Nullable User user1 = null;
        try{
            user1 = userEndpoint.findUserByLoginUser(tokenUser,"User");
        } catch (Exception e){
            Assert.assertTrue(e.getMessage().contains("Access denied"));
        }
        Assert.assertNull(user1);
    }

}