package ru.fadeev.tm.util;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.constant.DateConst;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {

    @Nullable
    public static XMLGregorianCalendar calendarConverter(@Nullable final Date date) throws Exception {
        if (date == null) return null;
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(date.getTime());
        return datatypeFactory.newXMLGregorianCalendar(gc);
    }

    @Nullable
    public static String dateToString(@Nullable final XMLGregorianCalendar gregorianDate) {
        if (gregorianDate == null)
            return "";
        Date date = gregorianDate.toGregorianCalendar().getTime();
        return DateConst.DATE_FORMAT.format(date);
    }

}
