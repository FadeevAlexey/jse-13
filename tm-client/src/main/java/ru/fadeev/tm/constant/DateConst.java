package ru.fadeev.tm.constant;


import java.text.SimpleDateFormat;

public class DateConst {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

}
