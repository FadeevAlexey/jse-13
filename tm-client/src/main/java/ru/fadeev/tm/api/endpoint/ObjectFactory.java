
package ru.fadeev.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.fadeev.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "Exception");
    private final static QName _FindAllUser_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "findAllUser");
    private final static QName _FindAllUserResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "findAllUserResponse");
    private final static QName _FindOneUser_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "findOneUser");
    private final static QName _FindOneUserResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "findOneUserResponse");
    private final static QName _FindUserByLoginUser_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "findUserByLoginUser");
    private final static QName _FindUserByLoginUserResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "findUserByLoginUserResponse");
    private final static QName _IsLoginExistUser_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "isLoginExistUser");
    private final static QName _IsLoginExistUserResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "isLoginExistUserResponse");
    private final static QName _MergeUser_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "mergeUser");
    private final static QName _MergeUserAdmin_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "mergeUserAdmin");
    private final static QName _MergeUserAdminResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "mergeUserAdminResponse");
    private final static QName _MergeUserResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "mergeUserResponse");
    private final static QName _PersistUser_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "persistUser");
    private final static QName _PersistUserResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "persistUserResponse");
    private final static QName _RemoveUser_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "removeUser");
    private final static QName _RemoveUserResponse_QNAME = new QName("http://endpoint.api.tm.fadeev.ru/", "removeUserResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.fadeev.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link FindAllUser }
     * 
     */
    public FindAllUser createFindAllUser() {
        return new FindAllUser();
    }

    /**
     * Create an instance of {@link FindAllUserResponse }
     * 
     */
    public FindAllUserResponse createFindAllUserResponse() {
        return new FindAllUserResponse();
    }

    /**
     * Create an instance of {@link FindOneUser }
     * 
     */
    public FindOneUser createFindOneUser() {
        return new FindOneUser();
    }

    /**
     * Create an instance of {@link FindOneUserResponse }
     * 
     */
    public FindOneUserResponse createFindOneUserResponse() {
        return new FindOneUserResponse();
    }

    /**
     * Create an instance of {@link FindUserByLoginUser }
     * 
     */
    public FindUserByLoginUser createFindUserByLoginUser() {
        return new FindUserByLoginUser();
    }

    /**
     * Create an instance of {@link FindUserByLoginUserResponse }
     * 
     */
    public FindUserByLoginUserResponse createFindUserByLoginUserResponse() {
        return new FindUserByLoginUserResponse();
    }

    /**
     * Create an instance of {@link IsLoginExistUser }
     * 
     */
    public IsLoginExistUser createIsLoginExistUser() {
        return new IsLoginExistUser();
    }

    /**
     * Create an instance of {@link IsLoginExistUserResponse }
     * 
     */
    public IsLoginExistUserResponse createIsLoginExistUserResponse() {
        return new IsLoginExistUserResponse();
    }

    /**
     * Create an instance of {@link MergeUser }
     * 
     */
    public MergeUser createMergeUser() {
        return new MergeUser();
    }

    /**
     * Create an instance of {@link MergeUserAdmin }
     * 
     */
    public MergeUserAdmin createMergeUserAdmin() {
        return new MergeUserAdmin();
    }

    /**
     * Create an instance of {@link MergeUserAdminResponse }
     * 
     */
    public MergeUserAdminResponse createMergeUserAdminResponse() {
        return new MergeUserAdminResponse();
    }

    /**
     * Create an instance of {@link MergeUserResponse }
     * 
     */
    public MergeUserResponse createMergeUserResponse() {
        return new MergeUserResponse();
    }

    /**
     * Create an instance of {@link PersistUser }
     * 
     */
    public PersistUser createPersistUser() {
        return new PersistUser();
    }

    /**
     * Create an instance of {@link PersistUserResponse }
     * 
     */
    public PersistUserResponse createPersistUserResponse() {
        return new PersistUserResponse();
    }

    /**
     * Create an instance of {@link RemoveUser }
     * 
     */
    public RemoveUser createRemoveUser() {
        return new RemoveUser();
    }

    /**
     * Create an instance of {@link RemoveUserResponse }
     * 
     */
    public RemoveUserResponse createRemoveUserResponse() {
        return new RemoveUserResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "findAllUser")
    public JAXBElement<FindAllUser> createFindAllUser(FindAllUser value) {
        return new JAXBElement<FindAllUser>(_FindAllUser_QNAME, FindAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "findAllUserResponse")
    public JAXBElement<FindAllUserResponse> createFindAllUserResponse(FindAllUserResponse value) {
        return new JAXBElement<FindAllUserResponse>(_FindAllUserResponse_QNAME, FindAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "findOneUser")
    public JAXBElement<FindOneUser> createFindOneUser(FindOneUser value) {
        return new JAXBElement<FindOneUser>(_FindOneUser_QNAME, FindOneUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "findOneUserResponse")
    public JAXBElement<FindOneUserResponse> createFindOneUserResponse(FindOneUserResponse value) {
        return new JAXBElement<FindOneUserResponse>(_FindOneUserResponse_QNAME, FindOneUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLoginUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "findUserByLoginUser")
    public JAXBElement<FindUserByLoginUser> createFindUserByLoginUser(FindUserByLoginUser value) {
        return new JAXBElement<FindUserByLoginUser>(_FindUserByLoginUser_QNAME, FindUserByLoginUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLoginUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "findUserByLoginUserResponse")
    public JAXBElement<FindUserByLoginUserResponse> createFindUserByLoginUserResponse(FindUserByLoginUserResponse value) {
        return new JAXBElement<FindUserByLoginUserResponse>(_FindUserByLoginUserResponse_QNAME, FindUserByLoginUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsLoginExistUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "isLoginExistUser")
    public JAXBElement<IsLoginExistUser> createIsLoginExistUser(IsLoginExistUser value) {
        return new JAXBElement<IsLoginExistUser>(_IsLoginExistUser_QNAME, IsLoginExistUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsLoginExistUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "isLoginExistUserResponse")
    public JAXBElement<IsLoginExistUserResponse> createIsLoginExistUserResponse(IsLoginExistUserResponse value) {
        return new JAXBElement<IsLoginExistUserResponse>(_IsLoginExistUserResponse_QNAME, IsLoginExistUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "mergeUser")
    public JAXBElement<MergeUser> createMergeUser(MergeUser value) {
        return new JAXBElement<MergeUser>(_MergeUser_QNAME, MergeUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUserAdmin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "mergeUserAdmin")
    public JAXBElement<MergeUserAdmin> createMergeUserAdmin(MergeUserAdmin value) {
        return new JAXBElement<MergeUserAdmin>(_MergeUserAdmin_QNAME, MergeUserAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUserAdminResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "mergeUserAdminResponse")
    public JAXBElement<MergeUserAdminResponse> createMergeUserAdminResponse(MergeUserAdminResponse value) {
        return new JAXBElement<MergeUserAdminResponse>(_MergeUserAdminResponse_QNAME, MergeUserAdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "mergeUserResponse")
    public JAXBElement<MergeUserResponse> createMergeUserResponse(MergeUserResponse value) {
        return new JAXBElement<MergeUserResponse>(_MergeUserResponse_QNAME, MergeUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "persistUser")
    public JAXBElement<PersistUser> createPersistUser(PersistUser value) {
        return new JAXBElement<PersistUser>(_PersistUser_QNAME, PersistUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "persistUserResponse")
    public JAXBElement<PersistUserResponse> createPersistUserResponse(PersistUserResponse value) {
        return new JAXBElement<PersistUserResponse>(_PersistUserResponse_QNAME, PersistUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "removeUser")
    public JAXBElement<RemoveUser> createRemoveUser(RemoveUser value) {
        return new JAXBElement<RemoveUser>(_RemoveUser_QNAME, RemoveUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.fadeev.ru/", name = "removeUserResponse")
    public JAXBElement<RemoveUserResponse> createRemoveUserResponse(RemoveUserResponse value) {
        return new JAXBElement<RemoveUserResponse>(_RemoveUserResponse_QNAME, RemoveUserResponse.class, null, value);
    }

}
