package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Project;
import ru.fadeev.tm.api.endpoint.Task;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;

public interface ITerminalService {

    @Nullable
    String readString();

    @Nullable
    XMLGregorianCalendar readDate() throws Exception;

    void println(@Nullable String string);

    void print(@Nullable String string);

    void println(@Nullable Task task);

    void println(@Nullable Project project);

    void printTaskList(@Nullable Collection<Task> tasks);

    void printProjectList(@Nullable Collection<Project> projects);

}
