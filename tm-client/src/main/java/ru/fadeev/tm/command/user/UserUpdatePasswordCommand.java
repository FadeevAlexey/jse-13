package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Exception_Exception;
import ru.fadeev.tm.api.endpoint.User;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.InvalidSessionException;
import ru.fadeev.tm.util.PasswordHashUtil;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-updatePassword";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User Password Changes.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String session = serviceLocator.getAppStateService().getToken();
        if (session == null || session.isEmpty()) throw new AccessDeniedException("Access denied");
        @Nullable final User currentUser = serviceLocator.getUserEndpoint().findOneUser(session);
        if (currentUser == null) throw new InvalidSessionException("You were logout. Please log in again");
        terminal.println("[UPDATE PASSWORD]");
        terminal.println("Enter new password");
        @Nullable final String newPassword = PasswordHashUtil.md5(terminal.readString());
        currentUser.setPasswordHash(newPassword);
        serviceLocator.getUserEndpoint().mergeUser(session, currentUser);
        terminal.println("[OK]\n");
    }

}