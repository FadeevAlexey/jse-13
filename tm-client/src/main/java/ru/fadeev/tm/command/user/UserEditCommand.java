package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.endpoint.User;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.util.PasswordHashUtil;

public final class UserEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit user profile";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @Nullable final String session = serviceLocator.getAppStateService().getToken();
        if (session == null || session.isEmpty()) throw new AccessDeniedException("Access denied");
        @Nullable final User currentUser = userEndpoint.findOneUser(session);
        if (currentUser == null) throw new Exception("Can't find user");
        terminal.println("[EDIT PROFILE]");
        terminal.println("ENTER NEW NAME OR PRESS ENTER");
        @Nullable final String name = terminal.readString();
        if (userEndpoint.isLoginExistUser(name))
            throw new Exception("User with same name already exist");
        terminal.println("ENTER NEW PASSWORD OR PRESS ENTER");
        @Nullable final String newPassword = PasswordHashUtil.md5(terminal.readString());
        if (newPassword != null && !newPassword.isEmpty()) currentUser.setPasswordHash(newPassword);
        if (name != null && !name.isEmpty()) currentUser.setLogin(name);
        userEndpoint.mergeUser(session, currentUser);
        terminal.println("[OK]\n");
    }

}