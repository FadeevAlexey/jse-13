package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.Task;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalTaskNameException;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[TASK CREATE]");
        terminal.println("ENTER NAME:");
        @Nullable final String name = terminal.readString();
        if (name == null || name.isEmpty()) throw new IllegalTaskNameException("name can't be empty");
        @NotNull final Task task = new Task();
        task.setName(name);
        serviceLocator.getTaskEndpoint().persistTask(token, task);
        terminal.println("[OK]\n");
        terminal.println("WOULD YOU LIKE EDIT PROPERTIES TASK? USE COMMAND task-edit\n");
    }

}