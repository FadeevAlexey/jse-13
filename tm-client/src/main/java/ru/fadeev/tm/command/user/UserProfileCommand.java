package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.User;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;

public final class UserProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show current profile.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String session = serviceLocator.getAppStateService().getToken();
        if (session == null || session.isEmpty()) throw new AccessDeniedException("Access denied");
        @Nullable final User user = serviceLocator.getUserEndpoint().findOneUser(session);
        if (user == null) throw new AccessDeniedException("Access denied");
        terminal.println("[USER PROFILE]");
        terminal.println(String.format(
                "name: %s, role: %s, id: %s",
                user.getLogin(),user.getRole(),user.getId()) + "\n");
        terminal.println("IF YOU'D LIKE UPDATE PROFILE USE COMMAND: user-edit\n");
    }

}