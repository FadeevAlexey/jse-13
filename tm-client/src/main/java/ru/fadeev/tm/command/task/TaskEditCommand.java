package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.endpoint.Status;
import ru.fadeev.tm.api.endpoint.Task;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalTaskNameException;

import javax.xml.datatype.XMLGregorianCalendar;

public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[TASK EDIT]");
        terminal.println("ENTER CURRENT NAME:");
        @Nullable final String name = terminal.readString();
        @Nullable final String taskId = taskEndpoint.findIdByNameTask(token, name);
        if (taskId == null) throw new IllegalTaskNameException("Can't find task");
        @Nullable final Task task = taskEndpoint.findOneTask(token, taskId);
        if (task == null) throw new IllegalTaskNameException("Can't find task");
        fillFields(task);
        taskEndpoint.mergeTask(token, task);
        terminal.println("[OK]\n");
        terminal.println("WOULD YOU LIKE ADD PROJECT TO TASK ? USE COMMAND task-addProject\n");
    }

    private void fillFields(@NotNull final Task task) throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.println("YOU CAN CHANGE PROPERTIES OR PRESS ENTER");
        terminal.println("CHOOSE AN STATUS DESIRED: Planned, In progress, Done");
        @Nullable String status = terminal.readString();
        if (status != null && !status.isEmpty()) task.setStatus(Status.fromValue(status));
        terminal.println("CHANGE DESCRIPTION:");
        @Nullable final String description = terminal.readString();
        terminal.println("CHANGE START DATE:");
        final @Nullable XMLGregorianCalendar startDate = terminal.readDate();
        terminal.println("CHANGE FINISH DATE:");
        final @Nullable XMLGregorianCalendar finishDate = terminal.readDate();
        if (description != null && !description.isEmpty()) task.setDescription(description);
        if (startDate != null) task.setStartDate(startDate);
        if (finishDate != null) task.setFinishDate(finishDate);
    }

}