package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[TASK LIST]");
        terminal.printTaskList(serviceLocator.getTaskEndpoint().findAllTask(token));
    }

}