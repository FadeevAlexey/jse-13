package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    Task findOneTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Task removeTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void persistTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "task") @Nullable Task task
    ) throws Exception;

    @WebMethod
    void mergeTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "task") @Nullable Task task
    ) throws Exception;


    @Nullable
    @WebMethod
    String findIdByNameTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> findAllTask(@WebParam(name = "token") String token) throws Exception;

    @WebMethod
    void removeAllTask(@WebParam(name = "token") String token) throws Exception;
    
    @NotNull
    @WebMethod
    Collection<Task> searchByNameTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> searchByDescriptionTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> findAllByProjectIdTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @WebMethod
    void removeAllByProjectIdTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @WebMethod
    void removeAllProjectTask(@WebParam(name = "token") String token) throws Exception;
    
    @NotNull
    @WebMethod
    Collection<Task> sortByStartDateTask(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> sortByFinishDateTask(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> sortByCreationTimeTask(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<Task> sortByStatusTask(@WebParam(name = "token") final String token) throws Exception;

}