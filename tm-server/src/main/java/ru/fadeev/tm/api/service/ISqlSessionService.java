package ru.fadeev.tm.api.service;

import org.apache.ibatis.session.SqlSessionFactory;

public interface ISqlSessionService {

    SqlSessionFactory getSqlSessionFactory() throws Exception;
}
