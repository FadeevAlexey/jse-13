package ru.fadeev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId}")
    Collection<Task> findAllByUserId(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE projectId = #{projectId} AND userId = #{userId}")
    Collection<Task> findAllByProjectId(
            @NotNull @Param("projectId") String projectId,
            @NotNull @Param("userId") String userId
    ) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task")
    List<Task> findAll() throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_task WHERE id = #{id}")
    Task findOne(@NotNull String id) throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_task WHERE userId = #{userId} and id = #{id}")
    Task findOneByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    ) throws SQLException;

    @Delete("DELETE FROM app_task WHERE id = #{id}")
    void remove(@NotNull String id) throws SQLException;

    @Insert("INSERT INTO app_task " +
            "VALUES (#{id},#{name},#{description},#{projectId}," +
            "#{startDate},#{finishDate},#{userId},#{status},#{creationTime})")
    void persist(Task task) throws Exception;

    @Update("UPDATE app_task " +
            "SET name = #{name}, description = #{description}, projectId = #{projectId}, " +
            "startDate = #{startDate}, finishDate = #{finishDate}, userId = #{userId}, " +
            "status = #{status}, creationTime = #{creationTime} " +
            "WHERE id = #{id}")
    void merge(@NotNull Task task) throws Exception;

    @Delete("DELETE FROM app_task")
    void removeAll() throws SQLException;

    @Delete("DELETE FROM app_task WHERE userId = #{userId}")
    void removeAllByUserId(@NotNull String userId) throws SQLException;

    @Nullable
    @Select("SELECT id FROM app_task WHERE userId = #{userId} and name = #{name}")
    String findIdByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    ) throws SQLException;

    @Select("DELETE FROM app_task WHERE userId = #{userId} AND projectId = #{projectId}")
    void removeAllByProjectId(
            @NotNull @Param("userId")  String userId,
            @NotNull @Param("projectId") String projectId
    ) throws SQLException;

    @Select("DELETE FROM app_task WHERE userId = #{userId} AND projectId IS NOT null")
    void removeAllProjectTask(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} AND name LIKE '%' #{string} '%'")
    Collection<Task> searchByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("string") String string
    ) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} AND description LIKE '%' #{string} '%'")
    Collection<Task> searchByDescription(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("string") String string) throws SQLException;

    @Delete("DELETE FROM app_task WHERE userId = #{userId} AND id = #{id}")
    void removeByIdTask(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    ) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY startDate")
    Collection<Task> sortAllByStartDate(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY finishDate")
    Collection<Task> sortAllByFinishDate(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY status")
    Collection<Task> sortAllByStatus(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task WHERE userId = #{userId} ORDER BY creationTime")
    Collection<Task> sortAllByCreationDate(@NotNull String userId) throws SQLException;

}