package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    List<Project> findAllProjectAdmin(@WebParam(name = "session") String session) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    Project removeProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void persistProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "project") @Nullable Project project
    ) throws Exception;

    @WebMethod
    void mergeProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "project") @Nullable Project project
    ) throws Exception;

    @WebMethod
    void removeAllProjectAdmin(
            @WebParam(name = "token") String token
    ) throws Exception;

    @Nullable
    @WebMethod
    String findIdByNameProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @NotNull
    @WebMethod
    List<Project> findAllProject(@WebParam(name = "token") String token) throws Exception;

    @WebMethod
    void removeAllProject(
            @WebParam(name = "token") String token) throws Exception;


    @NotNull
    @WebMethod
    Collection<Project> searchByNameProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Project> searchByDescriptionProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<Project> sortByStartDateProject(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<Project> sortByFinishDateProject(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<Project> sortByCreationTimeProject(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<Project> sortByStatusProject(@WebParam(name = "token") final String token) throws Exception;

}