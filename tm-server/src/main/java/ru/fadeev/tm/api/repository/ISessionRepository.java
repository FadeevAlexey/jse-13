package ru.fadeev.tm.api.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository {

    @NotNull
    @Select("SELECT * FROM app_session")
    List<Session> findAll() throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_session WHERE id = #{id}")
    Session findOne(@NotNull String id) throws SQLException;

    @Delete("DELETE FROM app_session WHERE id = #{id}")
    void remove(@NotNull String id) throws SQLException;

    @Insert("INSERT INTO app_session " +
            "VALUES (#{id},#{userId},#{signature},#{creationTime}, #{role})")
    void persist(Session session) throws Exception;

    @Update("UPDATE app_session " +
            "SET userId = #{userId}, signature = #{signature}, creationTime = #{creationTime}, role = #{role} " +
            "WHERE id = #{id}")
    void merge(@NotNull Session session) throws Exception;

    @Delete("DELETE FROM app_session")
    void removeAll() throws SQLException;

    @Select("SELECT * FROM app_session WHERE userId = #{id}")
    Session findByUserId(@NotNull String userId) throws SQLException;

    @Delete("DELETE FROM app_session WHERE signature = #{signature}")
    void removeSessionBySignature(@NotNull String signature) throws SQLException;

    @Select("SELECT EXISTS (SELECT id from app_session WHERE id = #{id})")
    boolean contains(@NotNull String sessionId) throws SQLException;

}