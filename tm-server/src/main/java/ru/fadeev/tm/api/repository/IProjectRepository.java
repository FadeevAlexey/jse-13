package ru.fadeev.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    @NotNull
    @Select("SELECT * FROM app_project")
    List<Project> findAll() throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_project WHERE id = #{id}")
    Project findOne(@NotNull String id) throws SQLException;

    @Delete("DELETE FROM app_project WHERE id = #{id}")
    void remove(@NotNull String id) throws SQLException;

    @Insert("INSERT INTO app_project " +
            "VALUES (#{id},#{name},#{description}, " +
            "#{startDate},#{finishDate},#{userId},#{status},#{creationTime})")
    void persist(Project project) throws Exception;

    @Update("UPDATE app_project " +
            "SET name = #{name}, description = #{description}, " +
            "startDate = #{startDate}, finishDate = #{finishDate}, userId = #{userId}, " +
            "status = #{status}, creationTime = #{creationTime} " +
            "WHERE id = #{id}")
    void merge(@NotNull Project project) throws Exception;

    @Delete("DELETE FROM app_project")
    void removeAll() throws SQLException;

    @Delete("DELETE FROM app_project WHERE userId = #{userId}")
    void removeAllByUserId(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId}")
    List<Project> findAllByUserId(@NotNull String userId) throws SQLException;

    @Nullable
    @Select("SELECT id FROM app_project WHERE userId = #{userId} and name = #{name}")
    String findIdByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("name") String name
    ) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} AND name LIKE '%' #{string} '%'")
    Collection<Project> searchByName(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("string") String string
    ) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} AND description LIKE '%' #{string} '%'")
    Collection<Project> searchByDescription(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("string") String string) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY startDate")
    Collection<Project> sortAllByStartDate(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY finishDate")
    Collection<Project> sortAllByFinishDate(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY status")
    Collection<Project> sortAllByStatus(@NotNull String userId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_project WHERE userId = #{userId} ORDER BY creationTime")
    Collection<Project> sortAllByCreationDate(@NotNull String userId) throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_project WHERE userId = #{userId} and id = #{id}")
    Project findOneByUserId(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    ) throws SQLException;

    @Delete("DELETE FROM app_project WHERE userId = #{userId} AND id = #{id}")
    void removeById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String projectId) throws SQLException;

}
