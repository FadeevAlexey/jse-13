package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @NotNull
    List<T> findAll() throws Exception;

    @Nullable
    T findOne(@Nullable String id) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void persist(@Nullable T t) throws Exception;

    void merge(@Nullable T t) throws Exception;

    void removeAll() throws Exception;

}