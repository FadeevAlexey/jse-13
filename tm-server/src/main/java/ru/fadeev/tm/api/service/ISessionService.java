package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.enumerated.Role;

public interface ISessionService extends IService<Session> {

    @Nullable
    String getToken(@Nullable String login, @Nullable String password) throws Exception;

    void closeSession(@Nullable String session) throws Exception;

    boolean contains(@Nullable String sessionId) throws Exception;

    @NotNull
    Session checkSession(@Nullable final String currentSession) throws Exception;

    @NotNull
    Session checkSession(String currentSession, @NotNull final Role role) throws Exception;

}