package ru.fadeev.tm.api.repository;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository{

    @NotNull
    @Select("SELECT * FROM app_user")
    List<User> findAll() throws SQLException;

    @Nullable
    @Select("SELECT * FROM app_user WHERE id = #{id}")
    User findOne(@NotNull String id) throws SQLException;

    @Delete("DELETE FROM app_user WHERE id = #{id}")
    void remove(@NotNull String id) throws SQLException;

    @Insert("INSERT INTO app_user VALUES (#{id},#{login},#{passwordHash},#{role})")
    void persist(User user) throws Exception;

    @Update("UPDATE app_user SET login = #{login}, passwordHash = #{passwordHash}, role = #{role}  WHERE id = #{id}")
    void merge(@NotNull User user) throws Exception;

    @Delete("DELETE FROM app_user")
    void removeAll() throws SQLException;

    @Select("SELECT EXISTS (SELECT login from app_user WHERE login = #{login})")
    boolean isLoginExist(@NotNull String login) throws SQLException;

    @Select("SELECT * FROM app_user WHERE login = #{login}")
    User findUserByLogin(@NotNull String login) throws SQLException;

}