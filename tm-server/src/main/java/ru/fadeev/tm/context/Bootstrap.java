package ru.fadeev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.api.service.*;
import ru.fadeev.tm.endpoint.*;
import ru.fadeev.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @Getter
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @Getter
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @Getter
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @Getter
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @Getter
    @NotNull
    private final ISqlSessionService sqlSessionService = new SqlSessionService(this);


    public void init() throws Exception {
        propertyService.init();
        start();
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @Nullable final String serverHost = propertyService.getServerHost();
        @Nullable final String serverPort = propertyService.getServerPort();

        @NotNull final String link = String.format(
                "http://%s:%s/%s?wsdl", serverHost, serverPort, name);
        System.out.println(link);
        Endpoint.publish(link, endpoint);
    }

    public void start() {
        registry(userEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(sessionEndpoint);
    }

}