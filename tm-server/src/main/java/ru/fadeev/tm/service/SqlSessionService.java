package ru.fadeev.tm.service;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.repository.ISessionRepository;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.ISqlSessionService;
import ru.fadeev.tm.api.service.ServiceLocator;

import javax.sql.DataSource;

public class SqlSessionService implements ISqlSessionService {

    @NotNull
    final ServiceLocator serviceLocator;

    public SqlSessionService(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public SqlSessionFactory getSqlSessionFactory() throws Exception {
        @Nullable final String user = serviceLocator.getPropertyService().getJdbcUserName();
        @Nullable final String password = serviceLocator.getPropertyService().getJdbcPassword();
        @Nullable final String url = serviceLocator.getPropertyService().getJdbcUrl();
        @Nullable final String driver = serviceLocator.getPropertyService().getJdbcDriver();
        final DataSource dataSource =
                new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}