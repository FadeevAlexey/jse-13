package ru.fadeev.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ISessionRepository;
import ru.fadeev.tm.api.service.ISessionService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.InvalidSessionException;
import ru.fadeev.tm.util.EncryptUtil;
import ru.fadeev.tm.util.PasswordHashUtil;
import ru.fadeev.tm.util.SignatureUtil;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    public SessionService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final SqlSession sqlsession = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = sqlsession.getMapper(ISessionRepository.class);
        @NotNull final List<Session> sessions = repository.findAll();
        sqlsession.close();
        return sessions;
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        @Nullable final Session session = repository.findOne(id);
        sqlSession.close();
        return session;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.remove(id);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("Can't remove session");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void persist(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.persist(session);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("Can't create session");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void merge(@Nullable final Session session) throws Exception {
        if (session == null) return;
        @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.merge(session);
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("Can't update session");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeAll();
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("Can't remove sessions");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void closeSession(@Nullable final String token) throws Exception {
        if (token == null) return;
        @NotNull final Session currentSession = decryptSession(token);
        if (currentSession.getSignature() == null) return;
        @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        try {
            repository.removeSessionBySignature(currentSession.getSignature());
            sqlSession.commit();
        } catch (SQLException e) {
            sqlSession.rollback();
            throw new Exception("Can't close session");
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean contains(@Nullable final String sessionId) throws Exception {
        if (sessionId == null) return false;
        @NotNull final SqlSession sqlSession = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
        @NotNull final boolean contains = repository.contains(sessionId);
        sqlSession.close();
        return contains;
    }

    @NotNull
    private Session openSession(@Nullable final String login, @Nullable String password) throws Exception {
        if (login == null || login.isEmpty()) throw new InvalidSessionException("Bad login or password");
        if (password == null || password.isEmpty()) throw new InvalidSessionException("Bad login or password");
        @Nullable final User user = serviceLocator.getUserService().findUserByLogin(login);
        if (user == null) throw new InvalidSessionException("Bad login or password");
        if (!PasswordHashUtil.md5(password).equals(user.getPasswordHash()))
            throw new InvalidSessionException("Bad login or password");
        Session session = new Session();
        session.setUserId(user.getId());
        session.setRole(user.getRole());
        session.setSignature(SignatureUtil.sign(session,
                serviceLocator.getPropertyService().getSessionSalt(),
                serviceLocator.getPropertyService().getSessionCycle()));
        persist(session);
        return session;
    }

   @Nullable
   @Override
    public String getToken (@Nullable final String login, @Nullable String password) throws Exception {
        @Nullable final Session session = openSession(login,password);
        return cryptSession(session);
    }


    @NotNull
    public Session checkSession(final String session) throws Exception {
        @NotNull final Session currentSession = decryptSession(session);
        final long currentTime = new Date().getTime();
        if (currentSession == null) throw new InvalidSessionException();
        if (!contains(currentSession.getId())) throw new InvalidSessionException("Invalid session");
        if (currentSession.getUserId() == null) throw new InvalidSessionException();
        if (currentSession.getSignature() == null) throw new InvalidSessionException();
        if (currentSession.getRole() == null) throw new InvalidSessionException();
        @NotNull final Session testSession = new Session();
        testSession.setSignature(currentSession.getSignature());
        testSession.setRole(currentSession.getRole());
        testSession.setId(currentSession.getId());
        testSession.setCreationTime(currentSession.getCreationTime());
        testSession.setUserId(currentSession.getUserId());
        @Nullable final String sessionSignature =
                SignatureUtil.sign(testSession,
                        serviceLocator.getPropertyService().getSessionSalt(),
                        serviceLocator.getPropertyService().getSessionCycle());
        @Nullable final String currentSessionSignature =
                SignatureUtil.sign(currentSession,
                        serviceLocator.getPropertyService().getSessionSalt(),
                        serviceLocator.getPropertyService().getSessionCycle());
        if (sessionSignature == null || currentSessionSignature == null)
            throw new InvalidSessionException("Invalid session");
        if (!sessionSignature.equals(currentSessionSignature)) throw new InvalidSessionException("Invalid session");
        if (currentSession.getCreationTime() - currentTime > serviceLocator.getPropertyService().getSessionLifetime())
            throw new InvalidSessionException("Invalid session");
        return currentSession;
    }

    @NotNull
    public Session checkSession(final String currentSession, @NotNull final Role role) throws Exception {
        if (currentSession == null) throw new InvalidSessionException("Invalid session");
        checkSession(currentSession);
        @NotNull final Session session = decryptSession(currentSession);
        if (session.getRole() != role) throw new AccessDeniedException("Access denied");
        return session;
    }

    private String cryptSession(@Nullable final Session session) throws Exception {
        @NotNull final String key = serviceLocator.getPropertyService().getSecretKey();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return EncryptUtil.encrypt(json, key);
    }

    private Session decryptSession(@Nullable final String cryptSession) throws Exception {
        @NotNull final String key = serviceLocator.getPropertyService().getSecretKey();
        @NotNull final String json = EncryptUtil.decrypt(cryptSession, key);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Session session = mapper.readValue(json, Session.class);
        return session;
    }

}