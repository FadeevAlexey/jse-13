package ru.fadeev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Project;

import java.sql.SQLException;
import java.util.*;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull List<Project> findAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final List<Project> projects = repository.findAll();
        session.close();
        return projects;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @Nullable final Project project = repository.findOne(id);
        session.close();
        return project;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @Nullable final Project project = repository.findOneByUserId(userId,id);
        session.close();
        return project;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.remove(id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove project");
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId ,@Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.removeById(userId,projectId);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove project");
        } finally {
            session.close();
        }
    }

    @Override
    public void persist(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
           repository.persist(project);
           session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't create project");
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(@Nullable final Project project) throws Exception {
        if (project == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.merge(project);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't update project");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove project");
        } finally {
            session.close();
        }
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @Nullable final String id = repository.findIdByName(userId, name);
        session.close();
        return id;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final List<Project> projects= repository.findAllByUserId(userId);
        session.close();
        return projects;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        try {
            repository.removeAllByUserId(userId);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove project");
        } finally {
            session.close();
        }
    }

    @NotNull
    public Collection<Project> sortByStartDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final Collection<Project> projects= repository.sortAllByStartDate(userId);
        session.close();
        return projects;
    }

    @NotNull
    public Collection<Project> sortByFinishDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final Collection<Project> projects= repository.sortAllByFinishDate(userId);
        session.close();
        return projects;
    }

    @NotNull
    public Collection<Project> sortByStatus(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final Collection<Project> projects= repository.sortAllByStatus(userId);
        session.close();
        return projects;
    }

    @NotNull
    public Collection<Project> sortByCreationDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final Collection<Project> projects= repository.sortAllByCreationDate(userId);
        session.close();
        return projects;
    }

    @Override
    @NotNull
    public Collection<Project> searchByName(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final Collection<Project> projects= repository.searchByName(userId,string);
        session.close();
        return projects;
    }

    @Override
    @NotNull
    public Collection<Project> searchByDescription(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IProjectRepository repository = session.getMapper(IProjectRepository.class);
        @NotNull final Collection<Project> projects= repository.searchByDescription(userId,string);
        session.close();
        return projects;
    }

}