package ru.fadeev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.api.repository.ITaskRepository;

import java.sql.SQLException;
import java.util.*;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull List<Task> findAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final List<Task> tasks = repository.findAll();
        session.close();
        return tasks;
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.persist(task);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't create task");
        } finally {
            session.close();
        }
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @Nullable final Task task = repository.findOne(id);
        session.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @Nullable final Task task = repository.findOneByUserId(userId, id);
        session.close();
        return task;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.remove(id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove task");
        } finally {
            session.close();
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeByIdTask(userId, taskId);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove task");
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.merge(task);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't update task");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove tasks");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeAllByUserId(userId);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove tasks");
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> projects = repository.findAllByUserId(userId);
        session.close();
        return projects;
    }

    @Override
    @NotNull
    public Collection<Task> findAllByProjectId(@Nullable final String projectId, @Nullable final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> projects = repository.findAllByProjectId(projectId, userId);
        session.close();
        return projects;
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @Nullable final String id = repository.findIdByName(userId, name);
        session.close();
        return id;
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeAllByProjectId(userId, projectId);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove task");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAllProjectTask(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        try {
            repository.removeAllProjectTask(userId);
        } catch (SQLException e) {
            session.rollback();
            session.commit();
            throw new Exception("Can't remove task");
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public Collection<Task> searchByName(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> projects = repository.searchByName(userId, string);
        session.close();
        return projects;
    }

    @NotNull
    @Override
    public Collection<Task> searchByDescription(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> tasks = repository.searchByDescription(userId, string);
        session.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByStartDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> tasks = repository.sortAllByStartDate(userId);
        session.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByFinishDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> tasks = repository.sortAllByFinishDate(userId);
        session.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByStatus(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> tasks = repository.sortAllByStatus(userId);
        session.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByCreationDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final ITaskRepository repository = session.getMapper(ITaskRepository.class);
        @NotNull final Collection<Task> tasks = repository.sortAllByCreationDate(userId);
        session.close();
        return tasks;
    }

}