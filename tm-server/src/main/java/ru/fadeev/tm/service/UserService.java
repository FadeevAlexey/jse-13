package ru.fadeev.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.api.repository.IUserRepository;

import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull List<User> findAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        @NotNull final List<User> users = repository.findAll();
        session.close();
        return users;
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        @Nullable final User user = repository.findOne(id);
        session.close();
        return user;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.remove(id);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove user");
        } finally {
            session.close();
        }
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.removeAll();
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't remove user");
        } finally {
            session.close();
        }
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if (user == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.persist(user);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't create user");
        } finally {
            session.close();
        }
    }

    @Override
    public void merge(@Nullable final User user) throws Exception {
        if (user == null) return;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        try {
            repository.merge(user);
            session.commit();
        } catch (SQLException e) {
            session.rollback();
            throw new Exception("Can't update user");
        } finally {
            session.close();
        }
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        final boolean isLoginExists = repository.isLoginExist(login);
        session.close();
        return isLoginExists;
    }

    @Nullable
    @Override
    public User findUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return null;
        @NotNull final SqlSession session = serviceLocator.getSqlSessionService().getSqlSessionFactory().openSession();
        @NotNull final IUserRepository repository = session.getMapper(IUserRepository.class);
        @Nullable final User user = repository.findUserByLogin(login);
        session.close();
        return user;
    }

    @Override
    public void setAdminRole(@Nullable final User user) throws Exception {
        if (user == null) return;
        user.setRole(Role.ADMINISTRATOR);
        merge(user);
    }

}