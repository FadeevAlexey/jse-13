package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IUserEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint() {
        super(null);
    }

    public UserEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public List<User> findAllUser(@WebParam(name = "session") final String session) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public @Nullable User findOneUser(@WebParam(name = "session") final String session) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(session);
        return serviceLocator.getUserService().findOne(currentSession.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable User removeUser(
            @WebParam(name = "token") final String token,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(token, Role.ADMINISTRATOR);
        serviceLocator.getUserService().remove(id);
        return null;
    }

    @Override
    @WebMethod
    public void persistUser(
            @WebParam(name = "user") final @Nullable User user
    ) throws Exception {
        serviceLocator.getUserService().persist(user);
    }

    @Override
    @WebMethod
    public void mergeUserAdmin(
            @WebParam(name = "session") final String session,
            @WebParam(name = "user") final @Nullable User user
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session,Role.ADMINISTRATOR);
        serviceLocator.getUserService().merge(user);
    }

    @Override
    @WebMethod
    public void mergeUser(
            @WebParam(name = "token") final String token,
            @WebParam(name = "user") final @Nullable User user
    ) throws Exception {
       Session session =  serviceLocator.getSessionService().checkSession(token);
       if (!session.getUserId().equals(user.getId())) return;
        serviceLocator.getUserService().merge(user);
    }

    @Override
    @WebMethod
    public boolean isLoginExistUser(
            @WebParam(name = "login") @Nullable final String login) throws Exception {
        return serviceLocator.getUserService().isLoginExist(login);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLoginUser(
            @WebParam(name = "session") final String session,
            @WebParam(name = "login") @Nullable final String login
    ) throws Exception {
        serviceLocator.getSessionService().checkSession(session, Role.ADMINISTRATOR);
        return serviceLocator.getUserService().findUserByLogin(login);
    }

}