package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ITaskEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint() {
        super(null);
    }

    public TaskEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public Task findOneTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().findOne(currentSession.getUserId(), taskId);
    }

    @Override
    @Nullable
    @WebMethod
    public Task removeTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        serviceLocator.getTaskService().remove(currentSession.getUserId(), taskId);
        return null;
    }

    @Override
    @WebMethod
    public void persistTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        if (task == null) return;
        task.setUserId(currentSession.getUserId());
        serviceLocator.getTaskService().persist(task);
    }

    @Override
    @WebMethod
    public void mergeTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        @NotNull Session session = serviceLocator.getSessionService().checkSession(token);
        if (session.getUserId().equals(task.getUserId()))
            serviceLocator.getTaskService().merge(task);
    }

    @Override
    @Nullable
    @WebMethod
    public String findIdByNameTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().findIdByName(currentSession.getUserId(), name);
    }
    
    @NotNull
    @Override
    @WebMethod
    public Collection<Task> findAllTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().findAll(currentSession.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTask(
            @WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        serviceLocator.getTaskService().removeAll(currentSession.getUserId());
    }
    
    @Override
    @NotNull
    @WebMethod
    public Collection<Task> searchByNameTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().searchByName(currentSession.getUserId(), string);
    }

    @Override
    @NotNull
    @WebMethod
    public Collection<Task> searchByDescriptionTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().searchByDescription(currentSession.getUserId(), string);
    }

    @Override
    @NotNull
    @WebMethod
    public Collection<Task> findAllByProjectIdTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().findAllByProjectId(projectId, currentSession.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllByProjectIdTask(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        serviceLocator.getTaskService().removeAllByProjectId(currentSession.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public void removeAllProjectTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        serviceLocator.getTaskService().removeAllProjectTask(currentSession.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> sortByStartDateTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().sortByStartDate(currentSession.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> sortByFinishDateTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().sortByFinishDate(currentSession.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> sortByCreationTimeTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().sortByCreationDate(currentSession.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Task> sortByStatusTask(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getTaskService().sortByStatus(currentSession.getUserId());
    }

}