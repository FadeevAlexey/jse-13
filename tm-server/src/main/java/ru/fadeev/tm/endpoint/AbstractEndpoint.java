package ru.fadeev.tm.endpoint;

import ru.fadeev.tm.api.service.ServiceLocator;

public class AbstractEndpoint {

    ServiceLocator serviceLocator;

    public AbstractEndpoint(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}