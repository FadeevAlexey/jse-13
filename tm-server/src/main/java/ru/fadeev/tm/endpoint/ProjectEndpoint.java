package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findAllProjectAdmin(@WebParam(name = "token") final String token) throws Exception {
        serviceLocator.getSessionService().checkSession(token, Role.ADMINISTRATOR);
        return serviceLocator.getProjectService().findAll();
    }

    @Override
    @Nullable
    @WebMethod
    public Project findOneProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getProjectService().findOne(currentSession.getUserId(), projectId);
    }

    @Override
    @Nullable
    @WebMethod
    public Project removeProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "id") @Nullable final String id
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        serviceLocator.getProjectService().remove(currentSession.getUserId(), id);
        return null;
    }

    @Override
    @WebMethod
    public void persistProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        if (project == null) return;
        project.setUserId(currentSession.getUserId());
        serviceLocator.getProjectService().persist(project);
    }

    @Override
    @WebMethod
    public void mergeProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        Session session = serviceLocator.getSessionService().checkSession(token);
        if (session.getUserId().equals(project.getUserId()))
            serviceLocator.getProjectService().merge(project);
    }

    @Override
    @WebMethod
    public void removeAllProjectAdmin(
            @WebParam(name = "token") final String token) throws Exception {
        serviceLocator.getSessionService().checkSession(token, Role.ADMINISTRATOR);
        serviceLocator.getProjectService().removeAll();
    }

    @Override
    @Nullable
    @WebMethod
    public String findIdByNameProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getProjectService().findIdByName(currentSession.getUserId(), name);
    }

    @Override
    @NotNull
    @WebMethod
    public List<Project> findAllProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getProjectService().findAll(currentSession.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        serviceLocator.getProjectService().removeAll(currentSession.getUserId());
    }

    @NotNull
   public Collection<Project> sortByStartDateProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getProjectService().sortByStartDate(currentSession.getUserId());
    }

    @NotNull
    public Collection<Project> sortByFinishDateProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getProjectService().sortByFinishDate(currentSession.getUserId());
    }

    @NotNull
   public Collection<Project> sortByCreationTimeProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getProjectService().sortByCreationDate(currentSession.getUserId());
    }

    @NotNull
   public Collection<Project> sortByStatusProject(@WebParam(name = "token") final String token) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getProjectService().sortByStatus(currentSession.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> searchByNameProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getProjectService().searchByName(currentSession.getUserId(), string);
    }

    @NotNull
    @Override
    @WebMethod
    public Collection<Project> searchByDescriptionProject(
            @WebParam(name = "token") final String token,
            @WebParam(name = "string") @Nullable final String string
    ) throws Exception {
        @NotNull final Session currentSession = serviceLocator.getSessionService().checkSession(token);
        return serviceLocator.getProjectService().searchByDescription(currentSession.getUserId(), string);
    }

}