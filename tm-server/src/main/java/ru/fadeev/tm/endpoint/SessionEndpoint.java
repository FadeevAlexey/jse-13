package ru.fadeev.tm.endpoint;

import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ISessionEndpoint;
import ru.fadeev.tm.api.service.ServiceLocator;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Setter
@WebService(endpointInterface = "ru.fadeev.tm.api.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(){
        super(null);
    }

    public SessionEndpoint(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @Nullable
    @WebMethod
    public String getToken(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password) throws Exception {
        return serviceLocator.getSessionService().getToken(login, password);
    }

    @Override
    @WebMethod
    public void closeSession(@WebParam(name = "token") @Nullable final String token) throws Exception {
        serviceLocator.getSessionService().closeSession(token);
    }

}